# LAB

O objetivo deste Lab é apresentar para os estudantes da UFSCar como fazemos o
monitoramento do LB do Object Storage.

Tecnologias utilizadas:

- Node Exporter (Data collector)
- Prometheus (Data source)
- Grafana (Dashboards)
- Docker

## Arquitetura do Lab

![arquiterura](images/arquitetura.png)

Node Exporter coleta as métricas que iremos analisar (uso de CPU e memória) e
expõe esses dados num endpoint. Com isso, o Prometheus extrai os dados desse
endpoint e armazena na sua base de dados. Por fim, Grafana faz queries para o
Prometheus para receber esses dados e usá-los nos dashboards.

## Instalação e configuração

### Node Exporter

Para instalar e executar o Node Exporter:

```
$ docker run -d -p 9100:9100 quay.io/prometheus/node-exporter
```

Para confirmar se funcionou basta acessar localhost:9100.

### Prometheus

Copiar o arquivo de configuração do Prometheus para um path absoluto genérico e
temporário (esse passo é só para facilitar a reprodução do lab, o comando pode
ser alterado):

```
$ cp ./conf/prometheus.yml /tmp/prometheus.yml
```

Para instalar e executar o Prometheus:

```
$ docker run -d -p 9090:9090 --name=prometheus -v /tmp/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus
```

Para confirmar se funcionou basta acessar localhost:9090.

### Grafana

Para instalar e executar o Grafana:

```
$ docker run -d -p 3000:3000 --name=grafana grafana/grafana-oss
```

Acesse localhost:3000 para confirmar se funcionou. O usuário padrão é "admin"
com senha "admin".

Tendo instalado tudo, agora é preciso adicionar o Prometheus como data source.
Para isso, siga os passos das imagens abaixo:

1. Adicionar base de dados.
![grafana_add_data_source](images/grafana_add_data_source.png)
2. Selecionar Prometheus como base de dados.
![grafana_select_data_source](images/grafana_select_data_source.png)
3. Adicionar URL do Prometheus **(só é aceito o endereço de IP, domínio não funciona)**.
![grafana_add_prometheus_url](images/grafana_add_prometheus_url.png)

## Dashboard

### Importação

Uma alternativa é utilizar a funcionalidade de importar dashboard. Passos para
importar o dashboard [node-exporter-full-dashboard](https://grafana.com/grafana/dashboards/12486-node-exporter-full/):

1. Baixar o JSON ou fazer o download do ID do dashboard.
2. Selecionar ação de criar um novo dashboard.
![grafana_create_dashboard](images/grafana_create_dashboard.png)
3. Selecionar ação de importar um novo dashboard.
![grafana_select_import_dashboard](images/grafana_select_import_dashboard.png)
4. Colar o ID copiado ou o JSON.
